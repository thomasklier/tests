#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::string;

string toHex(const string& s)
{
    std::ostringstream ss;

    for (auto c : s)
    {
        ss << " " << std::hex << std::setfill('0') << std::setw(2) << std::uppercase << static_cast<int>(reinterpret_cast<unsigned char&>(c));
    }

    return ss.str();
}

int main(int, char*[])
{
    std::vector<std::pair<string, string>> m{
        {"Nul", "\u0000"},
        {"Alert", "\0007"},
        {"Space", " "},
        {"Exclamation Mark", "!"},
        {"Latin Capital Letter A", "A"},
        {"Latin Small Letter A", "a"},
        {"Delete", "\u007F"},
        {"Latin Capital Letter A with Diaeresis", "Ä"},
        {"Euro Sign", "€"},
        {"Diaeresis", "¨"},
        {"Katakana Letter Tu", "ツ"},
        {"Copyright Sign", "©"},
        {"Earth Ground", "⏚"},
        {"Hourglass with Flowing Sand", "⏳"},
        {"Circled Latin Capital Letter A", "Ⓐ"},
        {"Box Drawings Double Down and Left", "╗"},
        {"Box Drawings Double Vertical", "║"},
        {"Box Drawings Up Double and Right Single", "╙"},
        {"White Right Pointing Index", "☞"},
        {"White Up Pointing Index", "☝"},
        {"Skull and Crossbones", "☠"},
        {"High Voltage Sign", "⚡"},
        {"Snowman Without Snow", "⛄"},
        {"Raised Hand", "✋"},
        {"Square Km Squared", "㎢"},
        {"Musical Symbol G Clef", "𝄞"},
        {"Domino Tile Horizontal-05-03", "🁗"},
        {"Ten of Spades", "🂪"},
        {"Playing Card King of Diamonds", "🃎"},
        {"Full Moon with Face", "🌝"},
        {"Nose", "👃"},
        {"Clapping Hands Sign", "👏"},
        {"Man", "👨"},
        {"Reversed Hand with Middle Finger Extended", "🖕"},
        {"Grinning Face", "😀"},
        {"See-No-Evil Monkey", "🙈"},
        {"Slightly Smiling Face", "🙂"},
        {"Empty Set", "∅"},
        {"Hair Space", " "},
        {"Three-Per-Em Space", " "},
        {"Three-Em Dash", "⸻"},
        {"Roman Numeral Nine", "Ⅸ"},
        {"Double Low Line", "‗"},
        {"Mathematical Bold Fraktur Capital K", "𝕶"},
        {"Mathematical Bold Fraktur Small E", "𝖊"},
        // https://www.nerdfonts.com/cheat-sheet
        {"nf-pl-left_hard_divider", ""},
        {"nf-pl-right_hard_divider", ""},
        {"nf-fa-steam", ""},
        {"nf-cod-merge", ""},
        {"nf-fa-heart", ""},
        {"nf-fae-real_heart", ""},
        {"nf-linux-tux", ""},
        {"nf-linux-archlinux", ""},
        {"nf-fa-windows", ""},
        {"nf-linux-ubuntu", ""},
        {"nf-fa-medkit", ""},
        {"nf-cod-gear", ""},
        {"nf-cod-settings_gear", ""},
        {"nf-cod-settings", ""},
        {"nf-fa-power_off", ""},
        {"nf-fa-flash", ""},
        {"nf-fa-user", ""},
        {"nf-cod-key", ""},
        {"nf-md-file_sign", "󱧃"},
        {"nf-md-signal_5g", "󰩯"},
        {"nf-md-format_letter_case", "󰬴"},
        {"nf-md-format_letter_case_upper", "󰬶"},
        {"nf-md-format_letter_case_lower", "󰬵"},
        {"nf-cod-graph_line", ""},
        {"nf-cod-json", ""},
        {"nf-cod-symbol_namespace", ""},
        {"nf-cod-symbol_parameter", ""},
        {"nf-cod-law", ""},
        {"nf-cod-lightbulb", ""},
        {"nf-cod-refresh", ""},
        {"nf-cod-save", ""},
        {"nf-cod-save_all", ""},
        {"nf-cod-search", ""},
        {"nf-custom-cpp", ""},
        {"nf-dev-java", ""},
        {"nf-dev-python", ""},
        {"nf-dev-rasberry_pi", ""},
        {"nf-dev-scala", ""},
        {"nf-dev-stackoverflow", ""},
        {"nf-dev-database", ""},
        {"nf-dev-terminal_badge", ""},
        {"", ""},
        {"", ""},
        {"", ""},
        {"", ""},
        {"", ""},};

    ulong maxLen = 0;
    for (const auto &p : m)
    {
        if (maxLen < p.first.length())
        {
            maxLen = p.first.length();
        }
    }

    for (const auto &p : m)
    {
        if (!p.first.empty())
        {
            std::cout << p.first << string(maxLen - p.first.length() + 2, ' ') << p.second << "   " << toHex(p.second) << '\n';
        }
    }
}
