QT       -= core

CONFIG   += c++17
LIBS += -lboost_date_time

TARGET = Test
TEMPLATE = app

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

SOURCES += main.cpp

FORMS    +=

OTHER_FILES +=

RESOURCES +=

LIBS += -lcryptopp
